#!/usr/bin/env zsh
set -euo pipefail

# ensure PATH
source /etc/zprofile
source ~/.zshrc

INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
ASG_NAME=$(curl -s http://169.254.169.254/latest/meta-data/tags/instance/aws:autoscaling:groupName)
REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/region)
LIFECYCLE_HOOK_NAME="graceful-shutdown" # https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/97c42a07bbb6ade2457c575e57c4e59e1d5d5a5a/aws/orka/main.tf#L364

while true; do
  TARGET_STATE=$(curl -s http://169.254.169.254/latest/meta-data/autoscaling/target-lifecycle-state)
  if [ "$TARGET_STATE" = "Terminated" ]; then
    echo "Lifecycle target changed to Terminated, starting graceful shutdown..."
    sudo launchctl bootout system/gitlab-runner

    echo "Waiting for gitlab-runner process to quit"
    while pgrep -q gitlab-runner; do
      sleep 5
    done

    echo "Unregistering all runners"
    gitlab-runner unregister --all-runners

    echo "Completing lifecycle"
    aws autoscaling complete-lifecycle-action --lifecycle-action-result CONTINUE --instance-id "$INSTANCE_ID" --lifecycle-hook-name "$LIFECYCLE_HOOK_NAME" --auto-scaling-group-name "$ASG_NAME" --region "$REGION"

    echo "Disabling system/graceful-termination service" # ensure this script is not restarted after exiting, before the instance is killed
    sudo launchctl disable system/graceful-termination
  fi
  sleep 5
done
