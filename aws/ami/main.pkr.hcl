packer {
  required_version = "~> 1.8.3"

  required_plugins {
    amazon = {
      version = ">= 1.1.4"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

// 888     888     d8888 8888888b.  8888888        d8888 888888b.   888      8888888888 .d8888b.
// 888     888    d88888 888   Y88b   888         d88888 888  "88b  888      888       d88P  Y88b
// 888     888   d88P888 888    888   888        d88P888 888  .88P  888      888       Y88b.
// Y88b   d88P  d88P 888 888   d88P   888       d88P 888 8888888K.  888      8888888    "Y888b.
//  Y88b d88P  d88P  888 8888888P"    888      d88P  888 888  "Y88b 888      888           "Y88b.
//   Y88o88P  d88P   888 888 T88b     888     d88P   888 888    888 888      888             "888
//    Y888P  d8888888888 888  T88b    888    d8888888888 888   d88P 888      888       Y88b  d88P
//     Y8P  d88P     888 888   T88b 8888888 d88P     888 8888888P"  88888888 8888888888 "Y8888P"

variable "keypair_private_key" {
  type = string
}
variable "keypair_name" {
  type = string
}
variable "host_resource_group_arn" {
  type = string
}
variable "license_configuration_arn" {
  type = string
}
variable "region" {
  type = string
}

//  .d8888b.   .d88888b.  888     888 8888888b.   .d8888b.  8888888888 .d8888b.
// d88P  Y88b d88P" "Y88b 888     888 888   Y88b d88P  Y88b 888       d88P  Y88b
// Y88b.      888     888 888     888 888    888 888    888 888       Y88b.
//  "Y888b.   888     888 888     888 888   d88P 888        8888888    "Y888b.
//     "Y88b. 888     888 888     888 8888888P"  888        888           "Y88b.
//       "888 888     888 888     888 888 T88b   888    888 888             "888
// Y88b  d88P Y88b. .d88P Y88b. .d88P 888  T88b  Y88b  d88P 888       Y88b  d88P
//  "Y8888P"   "Y88888P"   "Y88888P"  888   T88b  "Y8888P"  8888888888 "Y8888P"

source "amazon-ebs" "macos" {
  ami_name = format("macos-${source.name}-%s", formatdate("YYYYMMDDhhmmssZ", timestamp()))

  instance_type = "mac2.metal"
  region        = var.region

  ena_support   = true
  ebs_optimized = true

  ssh_keypair_name     = var.keypair_name
  ssh_private_key_file = var.keypair_private_key

  temporary_security_group_source_public_ip = true

  launch_block_device_mappings {
    device_name = "/dev/sda1"
    volume_size = 500 # Each macOS image is around 50GB when uncompressed
  }

  placement {
    host_resource_group_arn = var.host_resource_group_arn
    tenancy                 = "host"
  }

  license_specifications {
    license_configuration_request {
      license_configuration_arn = var.license_configuration_arn
    }
  }

  ssh_username = "ec2-user"
  ssh_timeout  = "1h" // AWS says time to readiness can be up to 40min for M1 instances, 15min for Intel. https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-mac-instances.html

  run_tags = {
    Name = "macos-${source.name}"
  }
  run_volume_tags = {
    Name = "macos-${source.name}"
  }
}

// 888888b.   888     888 8888888 888      8888888b.   .d8888b.
// 888  "88b  888     888   888   888      888  "Y88b d88P  Y88b
// 888  .88P  888     888   888   888      888    888 Y88b.
// 8888888K.  888     888   888   888      888    888  "Y888b.
// 888  "Y88b 888     888   888   888      888    888     "Y88b.
// 888    888 888     888   888   888      888    888       "888
// 888   d88P Y88b. .d88P   888   888      888  .d88P Y88b  d88P
// 8888888P"   "Y88888P"  8888888 88888888 8888888P"   "Y8888P"

build {
  name = "runner"

  # add as many of these blocks as needed for other OSes
  source "source.amazon-ebs.macos" {
    name = "12-monterey-arm64-runner"

    # overrides
    source_ami_filter {
      filters = {
        name                = "amzn-ec2-macos-12.*"
        root-device-type    = "ebs"
        virtualization-type = "hvm"
        architecture        = "arm64_mac"
      }
      most_recent = true
      owners      = ["100343932686"]
    }
  }

  error-cleanup-provisioner "breakpoint" {
    note = "If needed, waiting for you to investigate error. Connect to the instance with `ssh -i ${var.keypair_private_key} ec2-user@IP`"
  }

  provisioner "file" {
    source      = "gitlab-runner.plist"
    destination = "/Users/ec2-user/gitlab-runner.plist"
  }

  provisioner "file" {
    source      = "graceful-termination.sh"
    destination = "/Users/ec2-user/graceful-termination.sh"
  }

  provisioner "file" {
    source      = "graceful-termination.plist"
    destination = "/Users/ec2-user/graceful-termination.plist"
  }

  provisioner "file" {
    source      = "dumb-init-darwin-arm64-v1.2.5"
    destination = "/Users/ec2-user/dumb-init-darwin-arm64-v1.2.5"
  }

  # Base runner installation
  provisioner "shell" {
    inline_shebang = "/bin/zsh -euo pipefail" // Note that the default shell in macOS is ZSH
    inline = [
      // Echo commands
      "set -x",
      // Load zshrc (we're in a non-login, non-interactive shell here, since this is launched by packer as an executable shell script)
      "source /etc/zprofile",
      "source ~/.zshrc",
      // resize disk. new disk size is only visible after reboot
      "CONTAINER_ID=$(diskutil list physical external | awk '/Apple_APFS/ {print $7}')",
      "DISK_ID=$(echo \"$CONTAINER_ID\" | cut -d's' -f1-2)",
      "echo 'y' | sudo diskutil repairDisk \"$DISK_ID\"",
      "sudo diskutil apfs resizeContainer \"$CONTAINER_ID\" 0",
      // Setup language
      "echo export LANG=en_US.UTF-8 >> ~/.zshrc",
      "echo export LC_ALL=en_US.UTF-8 >> ~/.zshrc",
      // Disable spotlight
      "sudo mdutil -a -i off",
      // Update brew
      "brew update",
      // Install gitlab-runner
      "curl -OL https://gitlab-runner-downloads.s3.amazonaws.com/v15.4.0/binaries/gitlab-runner-darwin-arm64",
      "chmod +x gitlab-runner-darwin-arm64",
      "sudo mv gitlab-runner-darwin-arm64 /usr/local/bin/gitlab-runner",
      "gitlab-runner install",
      // using custom plist until https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29324 is fixed
      "rm ~/Library/LaunchAgents/gitlab-runner.plist",
      "sudo mv /Users/ec2-user/gitlab-runner.plist /Library/LaunchDaemons",
      "sudo chown root:wheel /Library/LaunchDaemons/gitlab-runner.plist",
      "sudo chmod 0600 /Library/LaunchDaemons/gitlab-runner.plist",
      "sudo launchctl enable system/gitlab-runner",
      "sudo launchctl bootstrap system /Library/LaunchDaemons/gitlab-runner.plist",
      // Disable osx-credential-helper to prevent git to store password in the keychain, which is not active without GUI login
      "git config --global --add credential.helper \"\"",
      // Install dumb-init
      "chmod +x /Users/ec2-user/dumb-init-darwin-arm64-v1.2.5",
      "sudo mv /Users/ec2-user/dumb-init-darwin-arm64-v1.2.5 /usr/local/bin/dumb-init",
      // Setup graceful termination
      "chmod +x /Users/ec2-user/graceful-termination.sh",
      "sudo mv /Users/ec2-user/graceful-termination.sh /usr/local/bin/graceful-termination",
      "sudo mv /Users/ec2-user/graceful-termination.plist /Library/LaunchDaemons",
      "sudo chown root:wheel /Library/LaunchDaemons/graceful-termination.plist",
      "sudo chmod 0600 /Library/LaunchDaemons/graceful-termination.plist",
      "sudo launchctl enable system/graceful-termination",
      "sudo launchctl bootstrap system /Library/LaunchDaemons/graceful-termination.plist",
    ]
  }

  # Orka build dependencies
  provisioner "shell" {
    inline_shebang = "/bin/zsh -euo pipefail" // Note that the default shell in macOS is ZSH
    inline = [
      // Echo commands
      "set -x",
      // Load zshrc (we're in a non-login, non-interactive shell here, since this is launched by packer as an executable shell script)
      "source /etc/zprofile",
      "source ~/.zshrc",
      // Install packer
      "curl -OL https://releases.hashicorp.com/packer/1.8.3/packer_1.8.3_darwin_arm64.zip",
      "unzip packer_1.8.3_darwin_arm64.zip",
      "rm -f packer_1.8.3_darwin_arm64.zip",
      "chmod +x packer",
      "sudo mv packer /usr/local/bin/",
      // Install build dependencies
      "brew tap gitlab/shared-runners https://gitlab.com/gitlab-org/ci-cd/shared-runners/homebrew.git",
      "brew install coreutils moreutils expect ruby@2.7 python@3.9 gitlab/shared-runners/sshpass docker-credential-helper-ecr",
      "echo export PATH=\\\"$(brew --prefix ruby@2.7)/bin:\\$PATH\\\" >> ~/.zshrc",
      "echo export PATH=\\\"$($(brew --prefix ruby@2.7)/bin/gem env gemdir)/bin:\\$PATH\\\" >> ~/.zshrc",
      "echo export PATH=\\\"$(brew --prefix python@3.9)/bin:\\$PATH\\\" >> ~/.zshrc",
      "echo export PATH=\\\"$(brew --prefix python@3.9)/libexec/bin:\\$PATH\\\" >> ~/.zshrc",
      "/opt/homebrew/bin/pip3 install pipenv",
      // Setup ecr credential helper, tart can use this even though it is initially for docker
      "mkdir -p ~/.docker",
      "echo '{\"credHelpers\":{\"915502504722.dkr.ecr.eu-west-1.amazonaws.com\":\"ecr-login\"}}' > ~/.docker/config.json",
    ]
  }

  # Cleanup
  provisioner "shell" {
    inline_shebang = "/bin/zsh -euo pipefail" // Note that the default shell in macOS is ZSH
    inline = [
      // Echo commands
      "set -x",
      // Load zshrc (we're in a non-login, non-interactive shell here, since this is launched by packer as an executable shell script)
      "source /etc/zprofile",
      "source ~/.zshrc",
      // Remove ec2-macos-init history so everything runs again when launching an instance from this AMI
      "sudo rm -rf /usr/local/aws/ec2-macos-init/instances/*",
      "cat /dev/null > .ssh/authorized_keys"
    ]
  }
}
