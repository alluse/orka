Orka repository internal runners
================================

This document describes the AWS infrastructure needed to execute builds in the Orka repository.

The infrastructure is provisioned with Terraform, with a state stored using the GitLab backend in this repository.

The AMIs are build by Packer using the code in `../ami`.

## Components

- Infrastructure is provisioned in the shared runner-saas Sandbox Cloud instance, in the `eu-west-1` region.
- The Terraform state is stored using the GitLab backend on gitlab.com
- AWS License Manager is used to provision and release dedicated hosts for `mac2.metal` instances, as per https://aws.amazon.com/blogs/compute/implementing-autoscaling-for-ec2-mac-instances/
- Dedicated hosts are automatically released when no instance is running on them and they're more than 24h old
- The runner registration token is stored in an encrypted SSM parameter at `orka.runner_registration_token`. Instances register with GitLab automatically on provision using this token.
- A number of ECR repositories are created to host the output images built by `tart` in the CI for this repository.
- A Launch Configuration is created to configure the runner instances:
  - use AMIs built by code in `../ami`
  - use an IAM role that gives access to the SSM parameter as well as Autoscaling Group Lifecycle Hook Completion
  - use a public IP, with the default security group allowing all traffic (todo restrict this)
  - allows accessing tags from instance metadata endpoint to reply to the lifecycle hook
  - provision instances in a Host Resource Grouup with an attached License Configuration, to automatically provision and delete mac dedicated hosts
- An Autoscaling Group is created to host the runner instances
  - Desired capacity is 2 at the moment
  - Makes use of lifecycle hooks to not raise errors while the macos instances are provisioning (40min+)

## Prerequisites

1. This document assumes you have stored your AWS account credentials in `aws-vault`, in a profile named `runner`. You will need to associate an MFA device to your account to get programmatic access to IAM resources.

1. `tfenv` should be installed and active in your shell

1. `VNC Viewer` (or alternative, but I tested a few and have had good luck with that one) should be installed for troubleshooting.

## Accessing the macOS runners via SSH

SSH access is not enabled on the macOS runners. They don't have SSH keys authorized, and no security group opening the SSH port is configured. To access the instance, you have to use Session Manager.

1. The Amazon `session-manager-plugin` needs to be installed `brew install session-manager-plugin`

1. Execute `aws-vault exec runner -- aws ssm start-session --region eu-west-1 --target i-0f1757d3d80e56f2a`

1. The default user is `ssm-user`. You might want to `sudo su -l ec2-user` to switch to the regular user.

## Terminating an instance

You should use the following, which will correctly go through graceful termination, as it triggers lifecycle hooks in the ASG.

```
aws-vault exec runner -- aws autoscaling terminate-instance-in-auto-scaling-group --no-should-decrement-desired-capacity --region eu-west-1 --instance-id "$INSTANCE_ID"
```

Terminating an instance through the API is not recommended, as it bypasses graceful termination. This is why termination protection is enabled on the instances. The following would happen if you didn't use the above command:

- When OSX receives the termination request, it start shutting down.
- `launchd` send a SIGTERM to the runner service, which we translate to SIGQUIT. This starts the graceful shutdown of the runner.
- AWS will wait around 10 minutes (not specified) for the instance to shutdown, before terminating it by force
- So if a job is running that takes more than 10min to terminate, it will be killed
