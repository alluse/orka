#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

if [[ -z ${BASE_IMAGE_NAME:-} ]]; then
  # For toolchain builds, this is set in the YAML to the starter OS image
  # For following stages, this can be:
  # - set by the `.base-image.env` artifact saved in the previous stage,
  #   for example `xcode` will use the image built previously
  #   in the same pipeline by the `toolchain` stage
  # - unset, in that case the previous stage was skipped by change detection
  #   and we should start off the main image for that previous stage
  export BASE_IMAGE_NAME="$PREVIOUS_IMAGE_TYPE.img"
fi

OUTPUT_IMAGE_NAME="$IMAGE_TYPE.img"
if [[ -z ${CI_COMMIT_BRANCH:-} || "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]]; then
  # for MRs, prefix the image name with the MR ID
  OUTPUT_IMAGE_NAME="mr-$CI_MERGE_REQUEST_IID-$OUTPUT_IMAGE_NAME"
fi

# replace TIMESTAMP in the output image name by the time of the pipeline start, with minute precision
# use pipeline start rather than job start so multiple images built from the same pipeline have the same version
TIMESTAMP=$(date --date "$CI_PIPELINE_CREATED_AT" "+%Y%m%d%H%M")
OUTPUT_IMAGE_NAME="${OUTPUT_IMAGE_NAME/TIMESTAMP/$TIMESTAMP}"

if [ ${#OUTPUT_IMAGE_NAME} -gt 29 ]; then
  echo "Error: \$OUTPUT_IMAGE_NAME $OUTPUT_IMAGE_NAME is longer than 29 chars."
  exit 1
fi

trap 'rm -f "$TMPFILE"' EXIT
TMPFILE=$(mktemp)

make orka-create-vm
make orka-deploy-vm TMPFILE="$TMPFILE"
VM_ID="$(jq -r .vm_id $TMPFILE)"
IP="$(jq -r .ip $TMPFILE)"
PORT="$(jq -r .ssh_port $TMPFILE)"
echo "Created VM with ID '$VM_ID'"

echo -n "Waiting for machine to boot..."
# uses socat to connect and wait for the server to say something, which will be the SSH version info:
while ! socat -T2 stdout "tcp:$IP:$PORT,connect-timeout=2,readbytes=3" 2>/dev/null | grep SSH >/dev/null; do
  sleep 1
  echo -n "."
done

echo "Running make orka-provision-$ROLE-vm with OUTPUT_IMAGE_NAME $OUTPUT_IMAGE_NAME"
make "orka-provision-$ROLE-vm" VM_ID="$VM_ID"

if make orka-check-image-exists OUTPUT_IMAGE_NAME="$OUTPUT_IMAGE_NAME"; then
  make orka-delete-vm-image OUTPUT_IMAGE_NAME="$OUTPUT_IMAGE_NAME";
  echo "Deleted pre-existing image, proceeding to save new image"
else
  echo "Pre-existing image not found, proceeding to save new image"
fi

# For all jobs except release use the seed password. Release job use the updated password.
if [[ $CI_JOB_STAGE == "release" ]]; then
  ANSIBLE_PASSWORD=$ANSIBLE_USER_UPDATED_PASSWORD
else
  ANSIBLE_PASSWORD=$ANSIBLE_USER_SEED_PASSWORD
fi

# There is a special case for xcode 10 on macos 10.14. Right after the install
# the VM freezes when asked to reboot. In that case, force restart it, wait for it to boot then
# try the normal reboot so ansible confirms it doesn't freeze a second time and waits for it to be fully online
echo "Rebooting machine to commit changes before saving image"
if [[ $IMAGE_TYPE == "xcode-10.14-10" ]]; then
  make orka-reboot-vm ANSIBLE_PASSWORD="$ANSIBLE_PASSWORD" || make orka-force-reboot-vm VM_ID="$VM_ID"
  sleep 120
fi

make orka-reboot-vm ANSIBLE_PASSWORD="$ANSIBLE_PASSWORD"
make orka-save-vm-image VM_ID="$VM_ID" OUTPUT_IMAGE_NAME="$OUTPUT_IMAGE_NAME"

echo "BASE_IMAGE_NAME=$OUTPUT_IMAGE_NAME" > .base-image.env

make orka-purge-vm

rm "$TMPFILE"
TMPFILE=$(mktemp)

make orka-create-vm BASE_IMAGE_NAME="$OUTPUT_IMAGE_NAME"
make orka-deploy-vm TMPFILE="$TMPFILE"
VM_ID="$(jq -r .vm_id $TMPFILE)"
IP="$(jq -r .ip $TMPFILE)"
PORT="$(jq -r .ssh_port $TMPFILE)"
echo "Created VM with ID '$VM_ID'"

echo -n "Waiting for machine to boot..."
# uses socat to connect and wait for the server to say something, which will be the SSH version info:
while ! socat -T2 stdout "tcp:$IP:$PORT,connect-timeout=2,readbytes=3" 2>/dev/null | grep SSH >/dev/null; do
  sleep 1
  echo -n "."
done
echo

LOGIN_PASSWORD=$ANSIBLE_PASSWORD TARGET_HOST=$IP TARGET_PORT=$PORT ROLES_TO_TEST="${ADDITONAL_SPECS:-},$ROLE" bundle exec rake spec

# Create production VM configuration
# TODO: this is broken since we have the prod-01 and prod-02 clusters.
# A copy of the image file from dev to prod-{01,02} needs to happen, then the command below needs to be executed on each cluster.
# The copy of the image could be automated given a container with sufficient disk space (60GB+). But it would be quite slow and
# require the credentials for the destination VPN endpoints.
# if [[ $CI_JOB_STAGE == "release" ]]; then
#   ORKA_VM_NAME="${OUTPUT_IMAGE_NAME%.img}"
#   ORKA_VM_NAME="${ORKA_VM_NAME//./-}"

#   ORKA_API_TOKEN=$ORKA_API_TOKEN_CANARY make orka-create-vm BASE_IMAGE_NAME="$OUTPUT_IMAGE_NAME" ORKA_VM_NAME="${ORKA_VM_NAME/ork/can}" CPU_COUNT="4" IO_BOOST="$IO_BOOST"

#   if [[ $IMAGE_TYPE != "ork-next" ]]; then
#     ORKA_API_TOKEN=$ORKA_API_TOKEN_BETA make orka-create-vm BASE_IMAGE_NAME="$OUTPUT_IMAGE_NAME" ORKA_VM_NAME="$ORKA_VM_NAME" CPU_COUNT="4" IO_BOOST="$IO_BOOST"
#   fi
# fi
