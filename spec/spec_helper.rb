# frozen_string_literal: true

require 'serverspec'
require 'pry'
require 'English'
require 'rspec/retry'

RSpec.configure do |config|
  # show retry status in spec process
  config.verbose_retry = true
  # show exception that triggers a retry if verbose_retry is set to true
  config.display_try_failure_messages = true
  # always retry failed specs, with 30sec interval. We're seeing some flaky specs that are raising timeouts accross OSes and specs
  config.default_retry_count = 3
  config.default_sleep_interval = 30
end

set :backend, :ssh
set :disable_sudo, true
set :os, { family: 'darwin' }
set :shell, '/bin/zsh'

HOST = ENV.fetch('TARGET_HOST')
PORT = ENV.fetch('TARGET_PORT')

options = {
  user: 'gitlab',
  port: PORT,
  user_known_hosts_file: '/dev/null',
  verify_host_key: :never
}

if ENV.key?('LOGIN_PASSWORD')
  options[:password] = ENV['LOGIN_PASSWORD']
  options[:auth_methods] = ['password']
end

set :host, HOST
set :ssh_options, options

def run_on_target(cmd)
  ssh = ENV.key?('LOGIN_PASSWORD') ? "sshpass -p #{ENV['LOGIN_PASSWORD']} ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no" : 'ssh'
  out = `#{ssh} -p #{PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@#{HOST} #{cmd}`
  raise "unable to run `#{cmd}` on target: #{out}" if $CHILD_STATUS.exitstatus != 0

  out
end

def with_fixtures
  rsync = ENV.key?('LOGIN_PASSWORD') ? "sshpass -p #{ENV['LOGIN_PASSWORD']} rsync" : 'rsync'
  with_tmpdir do |tmpdir|
    out = `#{rsync} -arvz -e 'ssh -p #{PORT} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null' --delete spec/fixtures/ gitlab@#{HOST}:#{tmpdir}`
    raise "unable to setup fixtures on target: #{out}" if $CHILD_STATUS.exitstatus != 0

    yield tmpdir
  end
end

def with_tmpdir
  tmpdir = command('mktemp -d').stdout.strip
  yield tmpdir
ensure
  command("rm -rf #{tmpdir}")
end

RSpec::Matchers.define :be_a_successful_cmd do |_expected|
  match do |actual|
    actual.exit_status == 0
  end
  failure_message do |actual|
    "expected that `#{actual.name}` would be successful, got the following output:\n#{actual.stdout}\n#{actual.stderr}"
  end
end

def next?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'next'
end

def monterey?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'monterey'
end

def big_sur?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'big-sur'
end

def catalina?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'catalina'
end

def mojave?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'mojave'
end

def high_sierra?
  ENV.fetch('TOOLCHAIN_VERSION', '') == 'high-sierra'
end

def machine_is_arm?
  run_on_target('uname -m').strip == 'arm64'
end
